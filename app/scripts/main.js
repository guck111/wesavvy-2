import Vue from 'vue';
import VueAwesomeSwiper from 'vue-awesome-swiper'; //carousel
import 'swiper/dist/css/swiper.css';  //carousel styles

import Slider from './components/Slider.vue';
import PartnersCarousel from './components/PartnersCarousel.vue';
import ReviewsCarousel from './components/ReviewsCarousel.vue';




Vue.use(VueAwesomeSwiper);

Vue.component('slider', Slider);
Vue.component('partnerscarousel', PartnersCarousel);
Vue.component('reviewscarousel', ReviewsCarousel);




new Vue({
    el: '#app',
    data(){
        return {
            test: 'hello'
        }
    },
});